import java.util.Scanner;
public class ApplianceStore{
	public static void main(String[]args){
		Scanner keyboard = new Scanner(System.in);
		Microwave[] appliances = new Microwave[4];
		
		for(int i = 0; i < appliances.length; i++){
			appliances[i] = new Microwave();
			System.out.println("What size is your microwave?");
			appliances[i].size = keyboard.nextDouble();
			System.out.println("How much does it cost?");
			appliances[i].cost = keyboard.nextDouble();
			System.out.println("How many watts does it use?");
			appliances[i].powerLevel = keyboard.nextInt();
		}
		
		System.out.println(appliances[3].size);
		System.out.println(appliances[3].cost);
		System.out.println(appliances[3].powerLevel);
		
		appliances[0].microwaving();
		appliances[0].cost();

	}
}